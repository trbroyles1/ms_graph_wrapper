module gitlab.com/trbroyles1/ms_graph_wrapper

go 1.14

require (
	gitlab.com/trbroyles1/golog v0.2.3
	gitlab.com/micronix-solutions/notifications v0.1.1
)
