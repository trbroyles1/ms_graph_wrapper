package ms_graph_wrapper

type GraphAPIConfig struct {
	TenantID       string
	AppID          string
	AppSecret      string
	Scope          string
	HTTPListenHost string
	HTTPListenPort string
	UseTLS         bool
	ProxyURL       string
}
