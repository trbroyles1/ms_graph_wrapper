/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package ms_graph_wrapper

//MailMessageWrapper defines the wrapper structure needed for a MailMessage sent to MS Graph. Not normally used outside of the graph api client
type MailMessageWrapper struct {
	Message         MailMessage `json:"message"`
	SaveToSentItems bool        `json:"saveToSentItems"`
}

//ContentTypeText - Message content types as defined by https://docs.microsoft.com/en-us/graph/api/resources/itembody?view=graph-rest-1.0
const (
	MessageContentTypeText = "text"
	MessageContentTypeHTML = "HTML"
)

const ()

//MailMessage defines the structure of a mail message to be sent via Graph
type MailMessage struct {
	ToRecipients []MailRecipient `json:"toRecipients"`
	//CcRecipients  []MailRecipient `json:"ccRecipients"`
	//BccRecipients []MailRecipient `json:"bccRecipients"`
	//ReplyTo       []MailRecipient `json:"replyTo"`
	Subject string   `json:"subject"`
	Body    ItemBody `json:"body"`
}

//MailRecipient represents information about a user in the sending or receiving end of an event, message or group post.
type MailRecipient struct {
	EmailAddress EmailAddress `json:"emailAddress"`
}

//EmailAddress defines the name and email address of a contact or message recipient.
type EmailAddress struct {
	Address string `json:"address"`
	Name    string `json:"name"`
}

//ItemBody defines the structure of the body of an item, such as a message, event or group post.
type ItemBody struct {
	ContentType string `json:"contentType"`
	Content     string `json:"content"`
}

//OneDriveItemCollection represents a collection of items in a OneDrive, a SharePoint site, or a folder within such a drive or site
type OneDriveItemCollection struct {
	Items []OneDriveItem `json:"value"`
}

//OneDriveItem represents a single item within a OneDriveItemCollection
type OneDriveItem struct {
	CreatedDateTime      string `json:"createdDateTime"`
	ETag                 string `json:"eTag"`
	ID                   string `json:"id"`
	Name                 string `json:"name"`
	LastModifiedDateTime string `json:"lastModifiedDateTime"`
	WebURL               string `json:"webUrl"`
}

// Deprecated: Please use MessageContentTypeText instead
const ContentTypeText = "text"

// Deprecated: Please use MessageContentTypeHTML instead
const ContentTypeHTML = "HTML"
