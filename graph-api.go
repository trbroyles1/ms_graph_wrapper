/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package ms_graph_wrapper

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

const msGraphEndpoint string = "https://graph.microsoft.com/v1.0"

//Client represents an instance of a client to the MS Graph API
type Client struct {
	Config GraphAPIConfig
	Token  *AccessToken
}

//AcquireToken obtains a new (auto-refreshing) token for this instance of the Graph API
func (apiInstance *Client) AcquireToken(r *AccessTokenRequest) (err error) {
	apiInstance.Token, err = NewAccessToken(&apiInstance.Config, r)
	return
}

/*GetSharepointList returns metadata for a Sharepoint list into the value pointed to by v. extraOptions is a string of the format specified for the "select" and "expand"
statements at https://docs.microsoft.com/en-us/graph/api/list-get?view=graph-rest-1.0&tabs=http
If the request to Graph is successful, error is nil and status and StatusCode contains the response string and code from the API indicating the status of the operation
If the request was unsuccessful, responseCode is 0 and err contains the resultant error
*/
func (apiInstance *Client) GetSharepointList(v interface{}, siteID string, listID string, extraOptions string) (err error) {
	resp, err := apiInstance.doGraphRequestWithExpectedStatusCode([]byte(""), msGraphEndpoint+"/sites/"+siteID+"/lists/"+listID+"/items?"+extraOptions, "GET", http.StatusOK)
	if err != nil {
		return
	}
	listDecoder := json.NewDecoder(resp.Body)
	err = listDecoder.Decode(v)
	return
}

/*CreateSharepointListItem creates a new item in a sharepoint list using the contents of the value pointed to by v.
If the request to Graph is successful, error is nil and status and StatusCode contains the response string and code from the API indicating the status of the operation
If the request was unsuccessful, responseCode is 0 and err contains the resultant error
*/
func (apiInstance *Client) CreateSharepointListItem(v interface{}, siteID string, listID string) (err error) {
	requestBody, err := json.Marshal(v)
	if err != nil {
		return
	}

	_, err = apiInstance.doGraphRequestWithExpectedStatusCode(requestBody, msGraphEndpoint+"/sites/"+siteID+"/lists/"+listID+"/items", http.MethodPost, http.StatusCreated)
	return
}

/*DeleteSharepointListItem deletes an item from a sharepoint list.
If the request to Graph is successful, error is nil and status and StatusCode contains the response string and code from the API indicating the status of the operation
If the request was unsuccessful, responseCode is 0 and err contains the resultant error
*/
func (apiInstance *Client) DeleteSharepointListItem(siteID string, listID string, itemID string) (err error) {
	_, err = apiInstance.doGraphRequestWithExpectedStatusCode([]byte(""), msGraphEndpoint+"/sites/"+siteID+"/lists/"+listID+"/items/"+itemID, http.MethodDelete, http.StatusNoContent)
	return
}

//SendMailMessage sends the mail message defined by message
func (apiInstance *Client) SendMailMessage(message MailMessage, saveToSentItems bool) (err error) {
	msgWrapper := MailMessageWrapper{message, saveToSentItems}
	requestBody, err := json.Marshal(msgWrapper)
	if err != nil {
		return
	}

	resp, err := apiInstance.DoGraphRequest(requestBody, msGraphEndpoint+"/me/sendMail", http.MethodPost)
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusAccepted {
		err = errors.New(resp.Status)
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(body))
		resp.Body.Close()
	}
	return
}

//GetSiteDriveRootContents returns the address of a OneDriveItemCollection representing the contents of the SharePoint site specified by siteID
func (apiInstance *Client) GetSiteDriveRootContents(siteID string) (*OneDriveItemCollection, error) {
	resp, err := apiInstance.doGraphRequestWithExpectedStatusCode([]byte(""), msGraphEndpoint+"/sites/"+siteID+"/drive/root/children", http.MethodGet, http.StatusOK)
	if err != nil {
		return nil, err
	}
	listDecoder := json.NewDecoder(resp.Body)
	contents := new(OneDriveItemCollection)
	err = listDecoder.Decode(contents)
	if err != nil {
		return nil, err
	}

	return contents, nil
}

//GetSiteFileDataStream returns an io.ReadCloser positioned at the start of a byte stream containing the contents of the file specified by siteID and itemID
func (apiInstance *Client) GetSiteFileDataStream(siteID string, itemID string) (io.ReadCloser, error) {
	resp, err := apiInstance.doGraphRequestWithExpectedStatusCode([]byte(""), msGraphEndpoint+"/sites/"+siteID+"/drive/items/"+itemID+"/content", http.MethodGet, http.StatusOK)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}

func (apiInstance *Client) doGraphRequestWithExpectedStatusCode(content []byte, endpoint string, method string, expectedStatusCode int) (*http.Response, error) {
	resp, err := apiInstance.DoGraphRequest(content, endpoint, method)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != expectedStatusCode {
		errDetails, _ := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		err = errors.New(resp.Status + " " + string(errDetails))
		return nil, err
	}
	return resp, nil
}

//DoGraphRequest makes an arbitrary HTTP request to the Graph API and returns a pointer to the resulting http.response
func (apiInstance *Client) DoGraphRequest(content []byte, endpoint string, method string) (resp *http.Response, err error) {
	// temp proxyURL, _ := url.Parse("http://172.25.1.20:8000") //TODO: better way to handle the proxy
	var c = &http.Client{Timeout: time.Duration(30) * time.Second} //TODO: configurable timeout

	if apiInstance.Config.ProxyURL != "" {
		proxyURL, _ := url.Parse(apiInstance.Config.ProxyURL)
		c.Transport = &http.Transport{Proxy: http.ProxyURL(proxyURL)}
	}

	req, err := http.NewRequest(method, endpoint, ioutil.NopCloser(bytes.NewBuffer(content)))
	if err != nil {
		return
	}
	req.Header.Add("Authorization", apiInstance.Token.TokenType+" "+apiInstance.Token.AccessToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err = c.Do(req)
	return
}
