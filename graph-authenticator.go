/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package ms_graph_wrapper

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sync"
	"time"

	logging "gitlab.com/trbroyles1/golog"
	"gitlab.com/micronix-solutions/notifications"
)

var refreshTokenWaitgroup *sync.WaitGroup

//AccessToken defines the fields returned by graph for an access token
type AccessToken struct {
	TokenType             string `json:"token_type"`
	Scope                 string `json:"scope"`
	ExpireInterval        int    `json:"expires_in"`
	AccessToken           string `json:"access_token"`
	RefreshToken          string `json:"refresh_token"`
	refreshShutdownSignal chan struct{}
}

//AccessTokenRequest defines the fields needed to request a new access token
type AccessTokenRequest struct {
	conf                       *GraphAPIConfig
	ExistingRefreshToken       string
	AuthURLHandler             func(string)
	Logger                     logging.Logger
	Notifier                   notifications.Notifier
	UpdatedRefreshTokenChannel chan<- string
	RefreshFailedCallback      func(error)
	fromFailedRequest          bool
}

//StopRefresh tells the token to stop refreshing
func (token *AccessToken) StopRefresh() {
	close(token.refreshShutdownSignal)
	refreshTokenWaitgroup.Wait()
}

//NewAccessToken builds a new access token for graph
func NewAccessToken(c *GraphAPIConfig, r *AccessTokenRequest) (token *AccessToken, err error) {
	r.conf = c
	if r.ExistingRefreshToken != "" {
		r.Logger.Info("Trying to gain access with provided refresh token")
		token, err = acquireAuthToken(r, "", r.ExistingRefreshToken)
	}
	if token == nil || token.AccessToken == "" {
		r.Logger.Warn("No / invalid refresh token, falling back to two step authentication")
		stepOneAuthorizationCode := acquireStepOneAuthCode(r)
		token, err = acquireAuthToken(r, stepOneAuthorizationCode, "")
	}
	if err == nil {
		wrapRefreshToken(token, r)
		refreshTokenWaitgroup = new(sync.WaitGroup)
		refreshTokenWaitgroup.Add(1)
	}
	return token, err

}

func wrapRefreshToken(token *AccessToken, req *AccessTokenRequest) {
	token.refreshShutdownSignal = make(chan struct{})
	go func() {
		tmr := time.NewTimer(time.Duration(token.ExpireInterval-100) * time.Second) //(20 * time.Second)
		//tmr := time.NewTimer(20 * time.Second)
		req.Logger.Debug(fmt.Sprintf("Token expiry in %d seconds, next token refresh in %d seconds", token.ExpireInterval, token.ExpireInterval-100))
		for {
			select {
			case <-tmr.C:
				req.Logger.Info("Refreshing token")
				//fmt.Println("Current token:", token.AccessToken)
				newToken, err := acquireAuthToken(req, "", token.RefreshToken)
				if err == nil {
					*token = *newToken
					req.Logger.Info("Refresh succeeded")
					if req.fromFailedRequest {
						notifications.SendNotification(req.Notifier, "Graph token refresh succeeded.")
						req.fromFailedRequest = false
					}
					wrapRefreshToken(token, req)
					//fmt.Println("new token:", token.AccessToken)
					return
				}
				req.Logger.Warn("Token refresh failed. Refresh will be attempted again in 90 seconds.")
				notifications.SendNotification(req.Notifier, "Graph token refresh failed.")
				req.fromFailedRequest = true
				tmr = time.NewTimer(90 * time.Second)
			case <-token.refreshShutdownSignal:
				req.Logger.Info("Shutting down token refresh")
				close(req.UpdatedRefreshTokenChannel)
				refreshTokenWaitgroup.Done()
				return
			}
		}
	}()
}

func acquireStepOneAuthCode(r *AccessTokenRequest) (stepOneAuthCode string) {
	authCodeChan := make(chan string)

	stepOneAuthorizationURL := `https://login.microsoftonline.com/` + r.conf.TenantID + `/oauth2/v2.0/authorize?` //TODO: put this in config / a builder of some kind
	stepOneAuthURLParameters := url.Values{}
	stepOneAuthURLParameters.Add("client_id", r.conf.AppID)
	stepOneAuthURLParameters.Add("response_type", "code")
	if r.conf.UseTLS {
		stepOneAuthURLParameters.Add("redirect_uri", "https://"+r.conf.HTTPListenHost+":"+r.conf.HTTPListenPort+"/receiveoauth")
	} else {
		stepOneAuthURLParameters.Add("redirect_uri", "http://"+r.conf.HTTPListenHost+":"+r.conf.HTTPListenPort+"/receiveoauth")
	}
	stepOneAuthURLParameters.Add("response_mode", "query")
	stepOneAuthURLParameters.Add("scope", r.conf.Scope)

	r.AuthURLHandler(stepOneAuthorizationURL + stepOneAuthURLParameters.Encode())

	authCallbackHandler := func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(w, "Authorized, please return to the app.")
		responseAuthCode := req.URL.Query()["code"][0]
		r.Logger.Info("got auth code")
		authCodeChan <- responseAuthCode
		close(authCodeChan)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/receiveoauth", authCallbackHandler)
	srv := http.Server{Addr: r.conf.HTTPListenHost + ":" + r.conf.HTTPListenPort, Handler: mux}
	go func() {
		var err error
		if r.conf.UseTLS {
			err = srv.ListenAndServeTLS("server.crt", "server.key")
		} else {
			err = srv.ListenAndServe()
		}
		if err != http.ErrServerClosed {
			r.Logger.Panic(err.Error())
		}
	}()

	stepOneAuthCode = <-authCodeChan
	srv.Shutdown(context.Background())
	return
}

func acquireAuthToken(r *AccessTokenRequest, authCode string, refreshToken string) (token *AccessToken, err error) {
	token = new(AccessToken)

	stepTwoAuthRequestURL := "https://login.microsoftonline.com/" + r.conf.TenantID + "/oauth2/v2.0/token"
	stepTwoAuthRequestParams := url.Values{}
	stepTwoAuthRequestParams.Add("tenant", r.conf.TenantID)
	stepTwoAuthRequestParams.Add("client_id", r.conf.AppID)
	stepTwoAuthRequestParams.Add("client_secret", r.conf.AppSecret)
	stepTwoAuthRequestParams.Add("scope", r.conf.Scope)

	if authCode == "" {
		stepTwoAuthRequestParams.Add("grant_type", "refresh_token")
		stepTwoAuthRequestParams.Add("refresh_token", refreshToken)
	} else {
		stepTwoAuthRequestParams.Add("grant_type", "authorization_code")
		stepTwoAuthRequestParams.Add("code", authCode)
	}

	if r.conf.UseTLS {
		stepTwoAuthRequestParams.Add("redirect_uri", "https://"+r.conf.HTTPListenHost+":"+r.conf.HTTPListenPort+"/receiveoauth")
	} else {
		stepTwoAuthRequestParams.Add("redirect_uri", "http://"+r.conf.HTTPListenHost+":"+r.conf.HTTPListenPort+"/receiveoauth")
	}

	c := &http.Client{}

	if r.conf.ProxyURL != "" {
		proxyURL, _ := url.Parse(r.conf.ProxyURL)
		c.Transport = &http.Transport{Proxy: http.ProxyURL(proxyURL)}
	}

	resp, err := c.PostForm(stepTwoAuthRequestURL, stepTwoAuthRequestParams)
	if err != nil {
		return
	}

	if resp.StatusCode != http.StatusOK {
		err = errors.New(resp.Status)
		return
	}
	defer resp.Body.Close()

	responseDecoder := json.NewDecoder(resp.Body)

	err = responseDecoder.Decode(token)
	if err == nil {
		r.UpdatedRefreshTokenChannel <- token.RefreshToken
	}

	return
}
